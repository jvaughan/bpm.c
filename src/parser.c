// -*-C-*-
// Heartbeat parser for BPM

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------

// IO
#include <stdio.h>
// isspace
#include <ctype.h>
// strncpy, strlen, strcat
#include <string.h>
// calloc, malloc
#include <stdlib.h>
// UCHAR_MAX
#include <limits.h>

// Header for this file
#include "parser.h"
// String utilities
#include "bpmstring.h"
// Field limits
#include "bpmlimits.h"

// Initializes Manifest
struct Manifest
initializemanifest (unsigned int n_metadata, unsigned int n_packages)
{
  // Return value
  struct Manifest manifest;
  // Loop counters
  unsigned int i_metadata = 0;
  unsigned int i_packages = 0;

  // Allocate metadata array and set n_metadata
  manifest.n_metadata = n_metadata;
  manifest.metadata = malloc (n_metadata);

  // Allocate metadata array values
  while (i_metadata < n_metadata)
    {
      // Set values to 0
      manifest.metadata[i_metadata] = calloc (metadata_length + 1,
					      sizeof (char));

      // Increment iterator
      i_metadata++;
    }

  // Allocate packages array and set n_packages
  manifest.n_packages = n_packages;
  manifest.packages = malloc (n_packages);

  // Allocate packages array values
  while (i_packages < n_packages)
    {
      // Set the i'th member of packages to \0.  Allocate 80 chars for the name,
      // and 1 char for the null terminator.
      manifest.packages[i_packages] = calloc (package_name_length + 1,
					      sizeof (char));

      i_packages++;
    }
  

  // Return initialized Manifest
  return manifest;
}

// Frees manifest memory
int
freemanifest (struct Manifest* manifest)
{
  // Metadata counter
  unsigned int i_metadata = 0;
  // Packages counter
  unsigned int i_packages = 0;

  // For every metadatum in the manifest
  while (i_metadata < manifest->n_metadata)
    {
      // Free memory
      free (manifest->metadata[i_metadata]);

      // Increment counter
      i_metadata++;
    }

  // Free metadata array memory
  free (manifest->metadata);

  // For every package in the manifest
  while (i_packages < manifest->n_packages)
    {
      // Free memory
      free (manifest->packages[i_packages]);

      // Increment counter
      i_packages++;
    }

  // Free packages array memory
  //free (manifest->packages);

  // Return success
  return 0;
}

// Parses Heartbeat from given file pointer
struct Manifest
parsehb (FILE** heartbeat_fp)
{  
  // Maximum line length for buffer
  unsigned char BUFFER_LIMIT = UCHAR_MAX;

  // Return value
  struct Manifest manifest;
  // String buffer
  char* buffer;
  // Holds result
  int result;
  // Metadata counter.  Starts at 3rd offset, since Package, Version, and Author
  // are expected to always be present.
  unsigned int i_metadata = 2;
  // Package counter
  unsigned int i_packages = 0;

  // Allocate zeroed memory for buffer
  buffer = calloc (BUFFER_LIMIT, sizeof (char));

  // Number of metadata lines
  unsigned int metalines = countmetalines (heartbeat_fp);

  // Number of metadata + package lines
  unsigned int totallines = countlines (heartbeat_fp);

  // Initialize manifest with line info from above
  if (metalines != 0 && totallines != 0)
    manifest = initializemanifest (metalines, totallines);
  else
    {
      // -20: Could not count lines of file
      fprintf (stderr, "-20: Could not count lines of file\n");

      // Return invalid manifest
      manifest.n_metadata = 0;
      goto EXIT;
    }

  // Unless there's an read error, parse
  while (fgets (buffer, BUFFER_LIMIT, *heartbeat_fp) != NULL)
    {
      // Don't modify address of actual buffer pointer, to allow for freeing
      // and to prevent segmentation faults.
      char* start = buffer;
      
      // Increment through whitespace
      while (isspace ((unsigned char) *start))
	start++;

      // Continue checking on next line if it's only whitespace
      if (*start == 0)
        continue;

      // Continue checking on next line if it's a comment (starts with #)
      if (*start == '#')
        continue;

      // Metadata starts with _
      if (*start == '_')
	{
	  // Trim trailing whitespace
	  trimtail (buffer);

	  // Set package name if not already set
	  if (*(start + 1) == 'P' && manifest.metadata[0][0] == 0)
	    {
	      strncpy (manifest.metadata[0],
		       nextword (start), // Ignore first word
		       metadata_value_length);

	      // Continue checking on next line
	      continue;
	    }
	  // Set package version if not already set
	  else if (*(start + 1) == 'V' && manifest.metadata[1][0] == 0)
	    {
	      strncpy (manifest.metadata[1],
		       nextword (start), // Ignore first word
		       metadata_value_length);

	      // Continue checking on next line
	      continue;
	    }
	  // Set package author if not already set
	  else if (*(start + 1) == 'A' && manifest.metadata[2][0] == 0)
	    {
	      strncpy (manifest.metadata[2],
		       nextword (start), // Ignore first word
		       metadata_value_length);

	      // Continue checking on next line
	      continue;
	    }
	  // Set arbitrary metadata
	  else
	    {
	      // Holds key.  Length of 16 + \t + \0 allowed.
	      char* key = malloc (18);
	      // Holds value.  Length of 80 + \0 allowed.
	      char* value = malloc (81);

	      // Copy 16 chars of key
	      strncpy (key,
		       (start + 1), // Take first word without "_"
		       wordlength (start + 1, metadata_key_length));

	      // Copy 80 chars of value
	      strncpy (value,
		       nextword (start), // Ignore first word
		       metadata_value_length);

	      // Append tab to key
	      strcat (key, "\t");

	      // Set metadata field to key\tvalue\0
	      strncpy (manifest.metadata[i_metadata],
		       strcat (key, value),
		       metadata_length + 1);

	      // Increment metadata counter
	      i_metadata++;

	      // Free memory
	      free (key);
	      free (value);

	      // Continue checking on next line
	      continue;
	    }
	}
      // Trim trailing whitespace and take it as a package by default
      else
	{
	  trimtail (buffer);
	  strncpy (manifest.packages[i_packages],
		   buffer,
		   package_name_length);
	  
	  // Increment package counter
	  i_packages++;

	  // Continue checking on next line
	  continue;
	}
    }

 EXIT:
  
  // Free allocated memory
  free (buffer);

  // Return manifest
  return manifest;
}
