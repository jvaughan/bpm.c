// -*-C-*-
// Main BPM routines

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

//------------------------------------------------------------------------------

// IO
#include <stdio.h>
// Booleans (ONLY C99+)
#include <stdbool.h>

// Header for this file
#include "main.h"
// Synchronize routine
#include "synchronize.h"

// Main Routine
//   Routes the program
int
main (int argc, char* argv[])
{
  // TODO: CLI options? Are they needed?

  return synchronize ();
}
