// -*-C-*-
// Collection of string utilities

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------

// isspace
#include <ctype.h>
// strlen
#include <string.h>
// fseek
#include <stdio.h>
// malloc
#include <stdlib.h>

// Header for this file
#include "bpmstring.h"

// Trims trailing whitespace in-place
int
trimtail (char* string)
{
  // End of string
  char* end = string + strlen (string) - 1;

  // Move end back
  while (end > string && isspace (*end))
    {
      end--;
    }

  // Write null-terminator
  *(end + 1) = 0;

  return 0;
}

// Points to next word of string
char*
nextword (char* string)
{
  // End of string
  char* end = string + strlen (string) - 1;
  // Target index
  char* target = string;

  // Skip until first whitespace
  while (end > target && !isspace (*target))
    {
      target++;
    }

  // Skip all whitespace until next word
  do
    {
      target++;
    }
  while (isspace (*target));

  // Return start of second word
  return target;
}

// Finds length of word
unsigned int
wordlength (char* string, unsigned int limit)
{
  // Target index
  char* target = string;

  // Move through line checking for space, up to limit - \0
  while ((target - string) < limit && !isspace (*target))
    {
      target++;
    }

  // Return length of word up to limit
  return target - string;
}

// TODO: Currently counts too high?  Reporting 12 on example Heartbeat.
//   countmetalines works fine.
// Counts total non-empty lines
unsigned int
countlines (FILE** fp)
{
  // Return value
  unsigned int lines = 0;
  // Check value
  int result = 0;
  // First character
  char firstchar = 0;
  // Character being checked
  char subject = 0;
  
  // Seek to beginning of file
  result = fseek (*fp, 0, SEEK_SET);
  if (result != 0)
    {
      // -12: Could not seek to start of file
      fprintf (stderr, "-12: Could not seek to start of file\n");
      
      return 0;
    }

  // Count number of lines that are not comments nor blank
  do
    {
      // Get initial character
      subject = fgetc (*fp);

      // If character is whitespace that is NOT nerwline
      if (isspace (subject) != 0 && subject != 0x0A)
	continue;

      // If character is newline
      else if (subject == 0x0A)
	{
	  lines++;
	  continue;
	}

      // If character is #
      /* Decrement lines, because this loop will continue through and will count
       * the newline at the end of the comment, even thought it should not count
       */
      else if (subject == 0x23)
	{
	  lines++;
	  continue;
	}
    }
  while (feof (*fp) == 0);

  // Seek back to beginning of file
  result = fseek (*fp, 0, SEEK_SET);
  if (result != 0)
    {
      // -12: Could not seek to start of file
      fprintf (stderr, "-12: Could not seek to start of file\n");

      return 0;
    }

  // Return number of lines in file
  return lines;
}

// Counts lines beginning with "_"
unsigned int
countmetalines (FILE** fp)
{
  // Return value
  unsigned int metalines = 0;
  // Check value
  int result = 0;
  // Line buffer
  char* buffer = malloc (255);

  // Seek to beginning of file
  result = fseek (*fp, 0, SEEK_SET);
  if (result != 0)
    {
      // -12: Could not seek to start of file
      fprintf (stderr, "-12: Could not seek to start of file\n");
      
      return 0;
    }

  // Unless end of file or read error, keep counting metalines
  while (feof (*fp) == 0 && fgets (buffer, sizeof(buffer), *fp) != NULL)
    {
      // Don't modify buffer itself, or it won't be freed
      char* start = buffer;
      
      // Skip whitespace
      while (isspace ((unsigned char) *start))
	start++;

      // If line starts with "_"
      if (*start == '_')
	metalines++;
    }

  // Free memory
  free (buffer);

  // Seek back to beginning of file
  result = fseek (*fp, 0, SEEK_SET);
  if (result != 0)
    {
      // -12: Could not seek to start of file
      fprintf (stderr, "-12: Could not seek to start of file\n");

      return 0;
    }

  // Return number of metalines in file
  return metalines;
}
