// -*-C-*-
// Collection of easily-changable limits

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------

// Include-guard
#ifndef BPMLIMITS_H
#define BPMLIMITS_H
#endif

// Length of metadata keys
#define metadata_key_length 16
// Length of metadata values
#define metadata_value_length 80

#define metadata_length (metadata_key_length + metadata_value_length)

// Length of package names
#define package_name_length 80
