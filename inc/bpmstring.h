// -*-C-*-
// Collection of string utilities

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------

// Include-guard
#ifndef BPMSTRING_H
#define BPMSTRING_H
#endif

// Trims trailing whitespace in place
int
trimtail (char*);

// Points to next word of string
char*
nextword (char* string);

// Finds length of word
unsigned int
wordlength (char*, unsigned int);

// Counts total non-empty lines
unsigned int
countlines (FILE**);

// Counts lines beginning with "_"
unsigned int
countmetalines (FILE**);
