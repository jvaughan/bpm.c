# -*-GNUmakefile-*-
# Master makefile for BPM

# BPM, a package manager for BLZ-OSPL
# Copyright (C) 2018 James Vaughan
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# For any questions, please do not hesitate to contact me through email
# at mailto:dev.jamesvaughan@gmail.com (Please send your GPG public key
# to allow for encrypted communication.)

# ------------------------------ Variables -------------------------------------

# Make search path
vpath	%.c	src/
vpath	%.h	inc/
vpath	%.texi	doc/

# Compile related variables
CFLAGS	= -std=gnu17 -Iinc -g
CC	= gcc

# Program related variables
OBJECTS	= main.o synchronize.o parser.o bpmstring.o

# Documentation related variables
DOCOBJS	= manual.dvi

# ------------------------------ Targets ---------------------------------------

# Default target, builds BPM
bpm:	$(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $@

# Builds documentation
doc:	$(DOCOBJS)

# --------------------------- Phony Targets ------------------------------------

.PHONY: clean

clean:
	rm -f $(OBJECTS) bpm manual*
